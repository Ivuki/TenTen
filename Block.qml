import QtQuick 2.15

Rectangle {
    id: block

    property int type: 0

    border { width: 0.5; color: Qt.darker(activePalette.button) }
    antialiasing: true
    radius: 4

    color: {
        if(type == 0) return "white";
        else if(type == 1) return "darkBlue";
        else if(type == 2) return "green";
        else return "red";
    }

    NumberAnimation on opacity {
        id: createAnimation
        from: 0
        to: 1
        duration: 3000
    }
    onColorChanged: {
        if(type == 1)
            createAnimation.start()
    }
}
