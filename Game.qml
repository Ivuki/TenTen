import QtQuick 2.15
import "GameLogic.js" as Game

Rectangle {
    id: screen

    property string text: "Game"

    width: parent.width;
    height: parent.height;
    color: "white"

    SystemPalette { id: activePalette }

    Rectangle {
        id: toolBar
        width: parent.width; height: 50
        color: activePalette.window
        anchors.top: parent.top

        Button {
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            text: "New Game"
            onClicked: Game.startNewGame()
        }

        Text {
            id: score
            color: "#FFFFFF"
            anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
            text: "Score: " + gameCanvas.score + "  Top: " + gameCanvas.topScore
        }

        Button {
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            text: "End Game"
            onClicked: gameOverDialog.showWithInput("GAME OVER!\n Please enter your name: ");

        }
    }

    Rectangle {
        id: sideBar
        width: 150;
        anchors {
            top: toolBar.bottom;
            bottom: parent.bottom;
            right: parent.right;
        }

        color: parent.color

        Text {
            id: nextBlockText
            color: "black"
            anchors{
                top:parent.top;
                topMargin: 50;
                horizontalCenter: parent.horizontalCenter;
            }
            text: "Next Block"
        }

        Item {
            id: nextBlockCanvas
            property int blockSize: 20

            anchors {
                top: nextBlockText.bottom;
                topMargin: 50;
                horizontalCenter: parent.horizontalCenter;
            }
            width: 60;
            height: 60;

            MouseArea {
                anchors.fill: parent;
            }
        }
    }

    Dialog {
        id: dialog
        anchors.centerIn: parent
        z: 100
    }

    Dialog {
        id: gameOverDialog
        anchors.centerIn: parent
        z: 100

        onClosed: {
            if (gameOverDialog.inputText != "")
                Game.saveHighScore(gameOverDialog.inputText);
        }
    }

    Item {

        anchors {
            top: toolBar.bottom;
            bottom: parent.bottom;
            left: parent.left;
            right: sideBar.left;
            margins: 20;
        }

        Image {
            id: background
            anchors.fill: parent
            source: "images/game-background.jpg"
            fillMode: Image.PreserveAspectCrop
        }

        Item {
            id: gameCanvas
            property int score: 0
            property int topScore: 0
            property int blockSize: 55

            anchors.centerIn: parent
            width: parent.width - (parent.width % blockSize);
            height: parent.height - (parent.height % blockSize);

            MouseArea {
                anchors.fill: parent;
                hoverEnabled: true;
                onPositionChanged: Game.handleHover(mouse.x, mouse.y);
                onClicked: Game.handleClick(mouse.x,mouse.y);
            }
        }
    }

}
