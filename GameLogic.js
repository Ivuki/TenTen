.import QtQuick.LocalStorage 2.15 as Sql
.import QtQuick 2.15 as Cute

var maxColumn = 10;
var maxRow = 10;
var maxIndex = maxColumn * maxRow;
var board = new Array(maxIndex);
var lastBoardState = new Array(maxIndex);
var nextBlockBoard = new Array(maxIndex);

var component;

var currentBlockIntex = 0;
var nextBlockIntex = 0;

var nextBlockProbability = [0.03, 0.06, 0.10, 0.14, 0.19, 0.24, 0.30, 0.40, 0.50, 0.60, 0.65, 0.75, 0.81, 0.86, 0.90, 0.94, 0.97, 1.0];

var lastColumn = 0;
var lastRow = 0;


/*
  Setup for a new game
  */
function startNewGame(){

    //hide dialgs shown
    gameOverDialog.hide();
    dialog.hide();

    //get curent score to 0
    gameCanvas.score = 0;
    gameCanvas.topScore = 0;

    //Get high score from file
    var db = Sql.LocalStorage.openDatabaseSync("SameGameScores", "1.0", "Local SameGame High Scores", 100);
    db.transaction(function(tx) {
        //In case the it is the 1st time playing the game
        tx.executeSql('CREATE TABLE IF NOT EXISTS Scores(name TEXT, score NUMBER, gridSize TEXT)');

        var rs = tx.executeSql('SELECT * FROM Scores ORDER BY score desc LIMIT 1');
        if(rs.rows.length > 0)
            gameCanvas.topScore = rs.rows.item(0).score
    });
    for(var i  = 0; i < maxIndex; ++i){
        lastBoardState[i] = 0;
    }

    drawGameBoard();

    currentBlockIntex = generateBlockIndex();
    generateNextBlock();
}

/*
  Generete the next block that the user will be placesing
  */
function generateNextBlock(){

    currentBlockIntex = nextBlockIntex;
    nextBlockIntex = generateBlockIndex();

    drawNextBlock();
    checkIsEndgame();
    return true;
}

/*
  Handle the click for users click
  */
function handleClick(xPos, yPos) {
    //calcupate position clicked
    var column = Math.floor(xPos / gameCanvas.blockSize);
    var row = Math.floor(yPos / gameCanvas.blockSize);

    //check if it's posible to set the block at the position
    if(checkPositionNotValid(column, row, currentBlockIntex)){
        return;
    }
    //if so place the block
    changeColor(column, row);
}

/*
  Handle the hover position changed for users
  */
function handleHover(xPos, yPos) {
    //calcupate position clicked
    var column = Math.floor(xPos / gameCanvas.blockSize);
    var row = Math.floor(yPos / gameCanvas.blockSize);

    if(checkIsLastPosition(column, row))
        return;

    restartLastPosition(column, row);

    var colorType = 2;
    if(checkPositionNotValid(column, row, currentBlockIntex)){
        colorType = 3;
    }

    //if so place the block
    changeColor(column, row, false, colorType);
}

/*
  Draws out the blocks on the canvas for the new game
  */
function drawGameBoard() {

    //Delete blocks from previous game
    for (var i = 0; i < maxIndex; ++i) {
        if (board[i] != null)
            board[i].destroy();
    }

    board = new Array(maxIndex);
    for (var column = 0; column < maxColumn; column++) {
        for (var row = 0; row < maxRow; row++) {
            board[calculateIndex(column, row)] = null;

            if (component == null)
                component = Qt.createComponent("Block.qml");

            if (component.status == Cute.Component.Ready) {

                var dynamicObject = component.createObject(gameCanvas);
                if (dynamicObject == null) {
                    console.log("error creating block");
                    console.log(component.errorString());
                    return false;
                }

                dynamicObject.type = 0;
                dynamicObject.x = (column * gameCanvas.blockSize);
                dynamicObject.y = (row * gameCanvas.blockSize);
                dynamicObject.width = gameCanvas.blockSize - 1;
                dynamicObject.height = gameCanvas.blockSize - 1;
                board[calculateIndex(column, row)] = dynamicObject;

            } else {
                console.log("error loading block component");
                console.log(component.errorString());
                return false;
            }
        }
    }

    return true;
}

/*
  Draw out the nextblock on the nextBlockCanvas in the side bar
  */
function drawNextBlock(){

    // clear old borad
    for (var i = 0; i < maxIndex; ++i) {
        if (nextBlockBoard[i] != null)
            nextBlockBoard[i].destroy();
    }

    //draw nex block on the canvas
    nextBlockBoard = new Array(maxIndex);
    for (var column = 0; column < nextBlock[nextBlockIntex][0].length; ++column) {
        for (var row = 0; row < nextBlock[nextBlockIntex].length; ++row) {
            if(nextBlock[nextBlockIntex][row][column] == 0)
                continue;

            if (component == null)
                component = Qt.createComponent("Block.qml");

            if (component.status == Cute.Component.Ready) {
                var dynamicObject = component.createObject(nextBlockCanvas);

                if (dynamicObject == null) {
                    console.log("error creating block");
                    console.log(component.errorString());
                    return false;
                }

                dynamicObject.type = 1;
                dynamicObject.x = (column * nextBlockCanvas.blockSize);
                dynamicObject.y = (row * nextBlockCanvas.blockSize);
                dynamicObject.width = nextBlockCanvas.blockSize - 1;
                dynamicObject.height = nextBlockCanvas.blockSize - 1;
                nextBlockBoard[calculateIndex(column, row)] = dynamicObject;

            } else {
                console.log("error loading block component");
                console.log(component.errorString());
                return false;
            }
        }
    }
}

/*
  place the nextBlock starting from a given checkPositionNotValid
  and call generateNextBlock
  */
function changeColor(column, row, genNext = true, colorType = 1) {

    if (board[calculateIndex(column, row)] == null)
        return;

    //go thru the block matrix for curent "nextBlock"
    for(var i = 0; i < nextBlock[currentBlockIntex][0].length; ++i){
        for(var j = 0; j < nextBlock[currentBlockIntex].length; ++j){
            if(nextBlock[currentBlockIntex][j][i] == 0)
                continue;

            board[calculateIndex(column + i, row + j)].type = colorType;

            if(colorType == 1)
                lastBoardState[calculateIndex(column + i, row + j)] = 1;

            if(genNext)
                gameCanvas.score += 1;
            checkCoumnAndRow(column + i , row + j);
        }
    }
    if(genNext)
        generateNextBlock();
}

/*
  Save hight score into local storage
  */
function saveHighScore(name) {

    var db = Sql.LocalStorage.openDatabaseSync("SameGameScores", "1.0", "Local SameGame High Scores", 100);
    var dataStr = "INSERT INTO Scores VALUES(?, ?, ?)";
    var data = [name, gameCanvas.score, maxColumn + "x" + maxRow];
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS Scores(name TEXT, score NUMBER, gridSize TEXT)');
        tx.executeSql(dataStr, data);

        var rs = tx.executeSql('SELECT * FROM Scores ORDER BY score desc LIMIT 5');
        var r = "\nHIGH SCORES\n\n"
        for (var i = 0; i < rs.rows.length; i++) {
            r += (i + 1) + ". " + rs.rows.item(i).name + ' got ' + rs.rows.item(i).score + ' points \n';
        }
        dialog.show(r);
    });
}

/*
  Funcions cheks for each Position if it's posible to set a block from it
  if it not posible for any of position, CAll gameOver dialog
  */
function checkIsEndgame(){
    for (var column = 0; column < maxColumn; ++column) {
        for (var row = 0; row < maxRow; ++row) {
            if(!checkPositionNotValid(column, row, currentBlockIntex)){
                return;
            }
        }
    }

    gameOverDialog.showWithInput("GAME OVER!\n Please enter your name: ");
}

/*
  the funcions checks for a given position and block index if it's posible to set the createCavasBlock
  */
function checkPositionNotValid(sColumn, sRow, nbIntex){

    for(var i = sColumn; i < sColumn + nextBlock[nbIntex][0].length; ++i){
        for(var j = sRow; j < sRow + nextBlock[nbIntex].length; ++j){

            //check only position that the nextBlock would ocupie the block
            if(nextBlock[nbIntex][j - sRow][i - sColumn] == 0)
                continue;

            //check that it doesn't go out of bounds
            if (i >= maxColumn || i < 0 || j >= maxRow || j < 0)
                return true;

            //check that the posion is not allready ocuied and that the position exists
            if (board[calculateIndex(i, j)] == null ||
                    lastBoardState[calculateIndex(i, j)] == 1)
                return true;
        }
    }
    return false;
}

/*
  check is a given row or column full,
  and if it is clear it
  */
function checkCoumnAndRow(column, row){

    var clearColumn = true;
    var clearRow = true;

    for(var i = 0; i < maxColumn; ++i){
        if(board[calculateIndex(i, row)].type != 1){
            clearRow = false;
            break;
        }
    }

    for(var i = 0; i < maxRow; ++i){
        if(board[calculateIndex(column, i)].type != 1){
            clearColumn = false;
            break;
        }
    }

    if(clearRow){
        for(var i = 0; i < maxColumn; ++i){
            board[calculateIndex(i, row)].type = 0
            lastBoardState[calculateIndex(i, row)] = 0
        }
        gameCanvas.score += 10;
    }

    if(clearColumn){
        for(var i = 0; i < maxRow; ++i){
            board[calculateIndex(column, i)].type = 0
            lastBoardState[calculateIndex(column, i)] = 0
        }
        gameCanvas.score += 10;
    }
    return;
}

function checkIsLastPosition(column, row){
    if(column == lastColumn && row == lastRow)
        return true;
    return false;
}

function restartLastPosition(column, row){
    if (board[calculateIndex(column, row)] == null)
        return;

    //go thru the block matrix for curent "nextBlock"
    for(var i = 0; i < maxColumn; ++i){
        for(var j = 0; j < maxRow; ++j){
            board[calculateIndex(i, j)].type = lastBoardState[calculateIndex(i, j)];
        }
    }
    lastColumn = column;
    lastRow = row;
    return;
}

function generateBlockIndex(){

    // take a random value and get the index for the nextBlock
    var seed = Math.random();
    var blockIntex = 0;
    while(1){
        if(seed < nextBlockProbability[blockIntex]){
            break;
        }
        ++blockIntex;
    }
    return blockIntex;
}

/*
  turn a 2D position into 1D index
  */
function calculateIndex(column, row) {
    return column + (row * maxColumn);
}

var nextBlock =
[[
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 1, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 1, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 0, 0, 0],
     [0, 1, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [0, 1, 0, 0, 0],
     [1, 1, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 0, 0, 0],
     [1, 1, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 1, 1, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [0, 0, 1, 0, 0],
     [0, 0, 1, 0, 0],
     [1, 1, 1, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 1, 0, 0],
     [0, 0, 1, 0, 0],
     [0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 1, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 1, 1, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 1, 1, 1, 1],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0]
 ],
[
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0],
     [1, 0, 0, 0, 0]
 ]
]
